'use strict';

// Load modules
// ======================================
var gulp         = require('gulp'),
	runSequence  = require('run-sequence'),
	sass         = require('gulp-sass'),
	autoprefixer = require('gulp-autoprefixer'),
	spritesmith  = require('gulp.spritesmith'),
	imagemin     = require('gulp-imagemin'),
	cleanCSS     = require('gulp-clean-css'),
	buffer       = require('vinyl-buffer');

// Variables
// ======================================
var folders = {
	sources: 'sources',

	sass_folder: 'sources/sass',
	sass_files: 'sources/sass/**/*.scss',
	sass_target: 'static/css',

	images: 'sources/images/*.{png,jpg,gif}',
	images_target: 'static/images/',

	sprites: 'sources/images/sprites/*.{png,jpg,gif}',
	sprites_retina: 'sources/images/sprites/*@2x.{png,jpg,gif}',
	sprites_target: 'static/images'
};

// $ gulp (run All tasks one by one)
// ======================================
gulp.task('default', function()
{
	runSequence(
		'sprite',
		'img-compress',
		'sass'
	);
});

gulp.task('watch', ['img-compress:watch', 'sass:watch']);


// Images
// ======================================
// Optimization
gulp.task('img-compress', function ()
{
	return gulp.src(folders.images)
		.pipe(buffer())
		.pipe(imagemin())
		.pipe(gulp.dest(folders.images_target));
});
gulp.task('img-compress:watch', function ()
{
	gulp.watch([folders.images], ['img-compress']);
});

// Sprites
gulp.task('sprite', function ()
{
	var spriteData = gulp.src(folders.sprites)
	.pipe(
		spritesmith({
			retinaSrcFilter: [folders.sprites_retina],
			retinaImgName: '../images/sprite@2x.png',
			imgName: '../images/sprite.png',
			cssName: '_sprite.scss',
			padding: 5,
			algorithm: 'top-down',
			cssTemplate: folders.sources + '/scss.template.handlebars',
			cssVarMap: function (sprite) {
				sprite.name = 'sprite-' + sprite.name
			}
		})
	);
	
	spriteData.img
		.pipe(buffer())
		.pipe(imagemin())
		.pipe(gulp.dest(folders.sprites_target));

	spriteData.css.pipe(
		gulp.dest(folders.sass_folder)
	);
});


// CSS
// ======================================
gulp.task('sass', function ()
{
	return gulp.src(
		folders.sass_files
	)
		// sass
		.pipe(sass({
			outputStyle: 'expanded'
		}))
		// autoprefixer
		.pipe(autoprefixer({
			browsers: ['last 5 version', 'ie >= 9', 'opera >= 12', '> 5%'],
			cascade: false
		}))
		.pipe(cleanCSS())
		.pipe(gulp.dest(folders.sass_target));
});
gulp.task('sass:watch', function ()
{
	gulp.watch([folders.sass_files], ['sass']);
});