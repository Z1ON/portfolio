// Background Parallax
$.fn.easyParallax = function(options)
{
	var defaults = {
		minWidth: 0
	};
	var settings = $.extend( {}, defaults, options );

	// disable parallax on "mobile" devices
	if (settings.minWidth >= $(window).width())
	{
		return;
	}

	return this.each(function()
	{
		var $target = $(this);

		makeParallax($target);

		$(window).scroll(function(e)
		{
			makeParallax($target);
		});
	});

	function makeParallax($block)
	{
		var scrolled    = $(window).scrollTop(),
			speed       = $block.data('speed') || '.4',
			offset      = $block.data('offset') || 0,
			offsetBlock = $block.data('offset-block'),
			css         = 0;

		if (offsetBlock)
		{
			css = (scrolled - $(offsetBlock).offset().top) * speed;
		}
		else if (offset > 0)
		{
			css = (scrolled - offset) * speed;
		}
		else
		{
			css = scrolled * speed;
		}

		$block.css('backgroundPosition','50% ' + css + 'px');
	}
};
// Background Paralax - end

// Scroll to
$.fn.scrollTo = function()
{
	return this.each(function()
	{
		$(this).on('click', function(e)
		{
			var $this       = $(this);
			var $target     = $($this.data('target') || 'body');
			var scrollSpeed = $this.data('speed') || 600;
			var offset      = $this.data('offset') || 0;

			if( $target.length )
			{
				e.preventDefault();

				$('html, body').stop().animate({
					scrollTop: $target.offset().top + offset
				}, scrollSpeed);
			}
		});
	});
};
// Scroll to - end

// Show "reflective"/"uv" images
$.fn.reflective = function(action)
{
	$(document).on('click', this.selector, function(e)
	{
		e.preventDefault();

		if (action == 'hide')
		{
			hide_images();
			
			return;
		}

		show_images();
	});

	$(window).on('resize', function()
	{
		if ($('body').hasClass('js-uv-loaded'))
		{
			recalculate_position();
		}
	});


	function show_images()
	{
		if ($('body').hasClass('js-uv-loaded'))
		{
			$('body').addClass('js-uv-show')

			return;
		}

		$('img[data-uv-src]').each(function()
		{
			// current "simple" image
			var $i = $(this);

			// create new "uv" image
			var uvimg = $('<img />',
			{
				src: $i.data('uv-src')
			})
				.load(function()
				{
					$(this).addClass('loaded');
				})
			.wrap($('<div />',
			{
				class: 'uv-portal__image js-uv-image',
			}))
				.parent()
				.css({
					width:  $i.width(),
					height: $i.height(),
					top:    $i.offset().top,
					left:   $i.offset().left
				})
				.appendTo('body');
		});

		$('body').addClass('js-uv-loaded js-uv-show');
	}

	function hide_images()
	{
		$('body').removeClass('js-uv-show');
	}

	function recalculate_position()
	{
		$('img[data-uv-src]').each(function()
		{
			$i = $(this);

			$('img[src="' + $i.data('uv-src') + '"]')
			.parent()
				.css({
					width:  $i.width(),
					height: $i.height(),
					top:    $i.offset().top,
					left:   $i.offset().left
				});
		});
	}
};
// Show "reflective"/"uv" images - end

// Product video player (via YouTube API)
function onYouTubePlayerAPIReady()
{
	var $container = $('.js-player');
	var player = new YT.Player('product-video', {
		events: {
			'onReady': function()
			{
				$container.on('click', function(e)
				{
					if ($container.hasClass('initializated'))
					{
						$container.addClass('play');

						player.playVideo();

						return;
					}

					// Move player to %visible% position (aka 50% 50%)
					$('html, body').stop().animate({
						scrollTop: $container.offset().top - ($(window).height() / 2 - $container.height() / 2)
					}, 500);

					player.playVideo();

					$container.addClass('initializated play');
				});
			},
			'onStateChange': function(e)
			{
				// on end
				if (e.data == 0)
				{
					$container.removeClass('play');
				}
			}
		}
	});
}
// Product video player - end

$(document).ready(function()
{
	$('html').removeClass('no-js');

	$(window).scroll(function()
	{
		var offTop = $(this).scrollTop();

		// Scroll to TOP button
		if (offTop > 500) $('.side-buttons__button--up').addClass('side-buttons__button--visible');
		else $('.side-buttons__button--up').removeClass('side-buttons__button--visible');
	});

	// Scroll spy
	$('.js-big-title').on('scrollSpy:enter', function()
	{
		$(this)
			.find('.js-big-title--text')
			.show();
	}).scrollSpy();
	// Scroll spy - end

	// Show resp navigation
	$('[data-toggle="head-menu"]').on('click', function(e)
	{
		e.preventDefault();

		$('body').toggleClass('open-phone-menu');
	});
	// Show resp navigation - end

	// UV
	$('[data-toggle="uv-show"]').reflective('show');
	$('[data-toggle="uv-hide"], .js-uv-image, .glow-in-dark').reflective('hide');
	// UV - end

	$('[data-toggle="parallax"]').easyParallax({
		minWidth: 767
	});
	$('[data-toggle="scroll"]').scrollTo();
});